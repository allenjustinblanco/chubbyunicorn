import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListExpenseComponent } from './list-expense/list-expense.component';
import { AddExpenseComponent } from './add-expense/add-expense.component';

const routes: Routes = [
  {path: 'expenses', component: ListExpenseComponent},
  {path: 'add-expense', component: AddExpenseComponent},
  {path: 'edit-expense/:id', component: AddExpenseComponent},
  {path: '', redirectTo: '/expenses', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
