export interface Expense {
    id: number,
    expense: string,
    amount: number,
    description: string 
}
