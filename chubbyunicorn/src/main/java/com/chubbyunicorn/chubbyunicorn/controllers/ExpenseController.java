package com.chubbyunicorn.chubbyunicorn.controllers;

import com.chubbyunicorn.chubbyunicorn.services.ExpenseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.chubbyunicorn.chubbyunicorn.models.Expense;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1")
public class ExpenseController {
    
    @Autowired
    ExpenseService expenseService;

    @GetMapping("/expenses")
    public ResponseEntity<List<Expense>> get() {
        List<Expense> expenses = expenseService.FindAll();
        return new ResponseEntity<List<Expense>>(expenses, HttpStatus.OK);
    }

}
