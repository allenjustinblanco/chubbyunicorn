package com.chubbyunicorn.chubbyunicorn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChubbyunicornApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChubbyunicornApplication.class, args);
	}

}
