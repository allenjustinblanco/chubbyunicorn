package com.chubbyunicorn.chubbyunicorn.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import com.chubbyunicorn.chubbyunicorn.models.Expense;

public interface ExpenseRepo extends JpaRepository<Expense, Long>{
    
}
