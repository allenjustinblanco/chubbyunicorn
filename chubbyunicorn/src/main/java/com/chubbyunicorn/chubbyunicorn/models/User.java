package com.chubbyunicorn.chubbyunicorn.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="users")
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue
    private long id;
    @Column(nullable = false, length =300, unique = true)
    private String username;
    @Column(nullable = false, length = 300, unique = true)
    private String email;
    @Column(nullable = false, length = 300)
    private String password;
    private String userFirstName;
    private String userLastName;

    public User() {
    }

    public User(long id, String username, String email, String password, String userFirstName, String userLastName) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
    }

    // copy constructor
    public User(User copy){
        id = copy.id;
        email = copy.email;
        username = copy.username;
        password = copy.password;
        userFirstName = copy.userFirstName;
        userLastName = copy.userLastName;

    }
}