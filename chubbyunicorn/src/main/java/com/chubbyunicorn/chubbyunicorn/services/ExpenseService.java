package com.chubbyunicorn.chubbyunicorn.services;

import java.util.List;

import com.chubbyunicorn.chubbyunicorn.models.Expense;

public interface ExpenseService {
    
    List<Expense> FindAll();

}
