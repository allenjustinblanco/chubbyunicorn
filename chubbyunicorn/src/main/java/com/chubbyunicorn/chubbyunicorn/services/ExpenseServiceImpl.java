package com.chubbyunicorn.chubbyunicorn.services;

import com.chubbyunicorn.chubbyunicorn.repos.ExpenseRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.chubbyunicorn.chubbyunicorn.models.Expense;

@Service
public class ExpenseServiceImpl implements ExpenseService {
    
    @Autowired
    ExpenseRepo expenseRepo;

    @Override
    public List<Expense> FindAll() {
        return expenseRepo.findAll();
    }

}
